import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App/App.js';
import * as serviceWorker from './serviceWorker';
import ReactGA from 'react-ga4';

const root = document.getElementById("root");

ReactDOM.render(
	<App />,
	root
);

// ReactGA.initialize('UA-148048608-1');
ReactGA.initialize('G-9P36FT3J02');
ReactGA.send("pageview");

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
