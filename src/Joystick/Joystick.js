import React, { Component } from 'react';
import Gestures from '../Gestures/Gestures.js'
import styles from './Joystick.module.scss';

class Joystick extends Component {

	constructor(props) {
		super(props);
		this.state = {
			x: 0,
			y: 0,
		};
	}

	onMoveStart = (data, event) => {
		event.preventDefault();
	}

	onMove = (data, event) => {
		const {x, y} = data;
		this.setState({
			x,
			y
		});
		this.props.onUpdate({x, y})
		event.preventDefault();
	}

	onMoveEnd = (data, event) => {
		const x = 0;
		const y = 0;
		this.setState({
			moving: false,
			x,
			y
		});
		this.props.onUpdate({x,y});
		event.preventDefault();
	}

	getControllerStyle = () => {
		const { x, y } = this.state;
		return {
			left: `calc(50% + ${x}px)`,
			top: `calc(50% + ${y}px)`
		}		
	}

	render() {
		return (
			<div className={styles.joystick}>
				<Gestures onMove={this.onMove} onMoveEnd={this.onMoveEnd}>
					<div style={this.getControllerStyle()} className={styles.joystickController}>
					{this.props.children}
					</div>
				</Gestures>
			</div>
		)
	}
}

export default Joystick;