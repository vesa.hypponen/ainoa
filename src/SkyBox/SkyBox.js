import React, { Component } from 'react';
import Cube from '../Cube/Cube.js'
import styles from './SkyBox.module.scss';

class SkyBox extends Component {

	constructor(props) {
		super(props);

		this.state = {
			dayCycle: 1000*30, // ms
			hour: 0
		};

		this.dayPhaseInterval = window.setInterval(this.updateHour, this.state.dayCycle/24)
	}

	componentWillUnmount() {
		window.clearInterval(this.dayPhaseInterval);
	}

	updateHour = () => {
		let { hour } = this.state;
		hour = hour+1;
		if (hour > 24) hour = 0;
		this.setState({
			hour: hour
		});
	}

	render() {
		const skyBoxSize = 1000; // in pixels
		const skyBoxStyle = {
			transform: `translate3d(${-skyBoxSize/2}px, ${-skyBoxSize/1.2}px, ${skyBoxSize/2}px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skewX(0deg) skewY(0deg)`,
		};
		// let { hour } = this.state;
		// hour = `0${hour}`.slice(-2);

		const skyBoxGradients = {};

		// const skyBoxGradients = {
		// 	front: styles[`sky-gradient-${hour}`],
		// 	back: styles[`sky-gradient-${hour}`],
		// 	left: styles[`sky-gradient-${hour}`],
		// 	right: styles[`sky-gradient-${hour}`],
		// }

		// const skyBoxGradients = {
		// 	front: styles['horizonGradient'],
		// 	back: styles['horizonGradient'],
		// 	left: styles['horizonGradient'],
		// 	right: styles['horizonGradient'],
		// 	top: styles['skyGradient'],
		// 	bottom: styles['groundGradient'],
		// }
		
		return (
			<div className={styles.skybox} style={skyBoxStyle}>
				<Cube key={`skybox`} styles={skyBoxGradients} size={skyBoxSize} position={{x:0,y:0,z:0}}/>
			</div>
		)
	}
}

export default SkyBox;