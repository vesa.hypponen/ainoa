import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import fscreen from 'fscreen';

// import logo from '../logo.svg';
import styles from './App.module.css';

import Home from '../Home/Home.js'
import Level from '../Level/Level.js'



function App() {
  
  const appRef = React.createRef();

  function onFullScreenClick () {
    fscreen.requestFullscreen(appRef.current);
  }

  const fsButton = fscreen.fullscreenEnabled ? <button onClick={onFullScreenClick}>FS</button> : null;

  return (
    <Router	basename={process.env.PUBLIC_URL}>
      <div ref={appRef} className={styles.App}>
        <header className={styles['App-header']}>
          {/* <img src={logo} className={styles['App-logo']} alt="logo" /> */}
          {/* <Link to="/">Home</Link>
          <Link to="/level">Level</Link> */}
          {fsButton}
        </header>

        <Route exact path="/" component={Home} />
        <Route path="/level/:user" component={Level} />
      </div>
    </Router>
  );
}

export default App;
