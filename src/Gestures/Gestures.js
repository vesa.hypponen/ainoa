import React, { Component } from 'react';
import styles from './Gestures.module.scss'

class Gestures extends Component {

	constructor(props) {
        super(props);

        this.longPressMs = 500;
        this.longPressTravelLimit = 25;

		this.velocityTolerance = 0.5; // in px/ms
		this.swipeTolerance = 25; // in px
		this.gesturePruneMs = 100; // for swipes, in ms
        
        this.touches = [];
		this.touchStartEvent = null;
        this.touchCurrentEvent = null;
        this.touchIndex = 0;
        
        this.useOnlyTouch = false;
        this.isMouseDown = false;
 
		this.gestureContainer = React.createRef();
	}

    componentDidMount() {
		const {current} = this.gestureContainer;
        this.touchStartListener = current.addEventListener('touchstart', this.touchStart, {passive: false});
        this.touchMoveListener = current.addEventListener('touchmove', this.touchMove, {passive: false});
        this.touchEndListener = current.addEventListener('touchend', this.touchEnd, {passive: false})

        this.mouseDownListener = current.addEventListener('mousedown', this.mouseDown, {passive: false});
        this.mouseMoveListener = current.addEventListener('mousemove', this.mouseMove, {passive: false});
        this.mouseUpListener = current.addEventListener('mouseup', this.mouseUp, {passive: false})

        // Prevent context menu
        this.contextMenuListener = current.addEventListener('contextmenu', (event) => event.preventDefault());
    }
  
    componentWillUnmount() {
		const {current} = this.gestureContainer;
        current.removeEventListener('touchstart', this.touchStartListener);
        current.removeEventListener('touchmove', this.touchMoveListener);
        current.removeEventListener('touchend', this.touchEndListener);

        current.removeEventListener('mousedown', this.mouseDownListener);
        current.removeEventListener('mousemove', this.mouseMoveListener);
        current.removeEventListener('mouseup', this.mouseUpListener)

        current.removeEventListener('contextmenu', this.contextMenuListener);

        window.clearTimeout(this.longPressTimer);

    }
  
    getVelocity = () => {
        if (this.touches.length < 3) {
            return {
                x: 0,
                y: 0
            }
        }
        const firstTouch = this.touches[this.touches.length-1];
        const lastTouch = this.touches[0];
        const xTravel = lastTouch.clientX - firstTouch.clientX;
        const yTravel = lastTouch.clientY - firstTouch.clientY;
        const travelTime = lastTouch.timestamp - firstTouch.timestamp;
        const xVelocity = xTravel / travelTime;
        const yVelocity = yTravel / travelTime;
        return {
            x: xVelocity,
            y: yVelocity
        }
    };

    getTotalTravel = () => {
        const firstTouch = this.touchStartEvent;
        const lastTouch = this.touchCurrentEvent;
        const xTravel = lastTouch.clientX - firstTouch.clientX;
        const yTravel = lastTouch.clientY - firstTouch.clientY;
        return {
            x: xTravel,
            y: yTravel
        }
    }

    longPressCB = () => {
        if (this.props.onLongPress) this.props.onLongPress();
    }

    stopLongPress = () => {
        console.log('stopLongPress');
        window.clearTimeout(this.longPressTimer);
        this.longPressTimer = null;
        if (this.props.onLongPressCancelled) this.props.onLongPressCancelled();
    }

    handleMoveStart = (nativeEvent, event) => {
        this.touchStartEvent = event;
        this.touchCurrentEvent = event;
        if (this.props.onMoveStart) {
            this.props.onMoveStart({
                absoluteX: this.touchStartEvent.clientX,
                absoluteY: this.touchStartEvent.clientY
            }, nativeEvent);
        }
        this.longPressTimer = window.setTimeout(this.longPressCB, this.longPressMs);
    }

    handleMove = (nativeEvent, event) => {
        if (!this.touchStartEvent) return;
        const pruneMs = this.gesturePruneMs;
        this.touches.push(Object.assign(event, {
            timestamp: Date.now()
        }));
        this.touches = this.touches.filter( (touch) => {
            return touch.timestamp > (Date.now()-pruneMs);
        });
        this.touchCurrentEvent = event;

        const {x: xVelocity, y: yVelocity} = this.getVelocity();
        const {x, y} = this.getTotalTravel();

        if (this.longPressTimer && (Math.abs(x) > this.longPressTravelLimit || Math.abs(x) > this.longPressTravelLimit)) {
            this.stopLongPress();
            return;
        }

        if (this.props.onMove) {
            this.props.onMove({
                x,
                y,
                xVelocity,
                yVelocity,
                absoluteX: this.touchCurrentEvent.clientX,
                absoluteY: this.touchCurrentEvent.clientY
            }, nativeEvent);

        }

    }

    handleMoveEnd = (nativeEvent, event) => {
        if (!this.touchStartEvent) return;
        const {x: xVelocity, y: yVelocity} = this.getVelocity();
        const xAbsVelocity = Math.abs(xVelocity);
        const yAbsVelocity = Math.abs(yVelocity);
        const {x, y} = this.getTotalTravel();
        const xAbsTravel = Math.abs(x);
        const yAbsTravel = Math.abs(y);
        let swipe = '';

        // If velocity > tolerance
        if ((xAbsVelocity > this.velocityTolerance || yAbsVelocity > this.velocityTolerance) &&
            (xAbsTravel > this.swipeTolerance || yAbsTravel > this.swipeTolerance)) {
            if (xAbsVelocity > yAbsVelocity) {
                if (xVelocity > 0) {
                    swipe = 'right';
                } else {
                    swipe = 'left';
                }
            } else {
                if (yVelocity > 0) {
                    swipe = 'down';
                } else {
                    swipe = 'up';
                }
            }
        }

        if (this.props.onMoveEnd) {
            this.props.onMoveEnd({
                x,
                y,
                xVelocity,
                yVelocity,
                absoluteX: this.touchCurrentEvent.clientX,
                absoluteY: this.touchCurrentEvent.clientY,
                swipe: swipe,
            }, nativeEvent);
        }
        this.touchStartEvent = null;
        this.touchCurrentEvent = null;
        this.touches = [];

        if (this.longPressTimer) this.stopLongPress();
    }

    mouseDown = (event) => {
        if (this.useOnlyTouch) return;
        console.log('real mouse');
        this.isMouseDown = true;
        this.handleMoveStart(event, event);
    }

    mouseMove = (event) => {
        if (!this.isMouseDown) return;
        this.handleMove(event, event);
    }

    mouseUp = (event) => {
        this.handleMoveEnd(event, event);
    }

    touchStart = (event) => {
        console.log('touchStart', event);
        this.useOnlyTouch = true;
        this.handleMoveStart(event, event.targetTouches[this.touchIndex]);

        // Prevent both touchstart and mouseDown
        // event.stopPropagation(); 
        // event.preventDefault(); 
    }

    touchMove = (event) => {
        this.handleMove(event, event.targetTouches[this.touchIndex]);
    };

    touchEnd = (event) => {
        this.handleMoveEnd(event, event.targetTouches[this.touchIndex]);
    };
  
    render() {
        return (
            <div ref={this.gestureContainer} className={styles.GestureContainer}>
                {this.props.children}
            </div>
        )
    }
}

export default Gestures;