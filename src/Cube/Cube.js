import React from 'react';
import styles from './Cube.module.scss';
import classnames from 'classnames';

function Cube (props) {
	let {size} = props;

	return (
		<div className={classnames({
			[styles.cube]: true,
			[props.styles]: Boolean(props.styles),
			[props.className]: Boolean(props.className)
			})} style={{ width: `${size}px`, height: `${size}px` }}>
			<div className={styles['front']} style={{transform: `rotateY(0deg) translateZ(${size/2}px)`}}/>
			<div className={styles['right']} style={{transform: `rotateY(90deg) translateZ(${size/2}px)`}}/>
			<div className={styles['back']} style={{transform: `rotateY(180deg) translateZ(${size/2}px)`}}/>
			<div className={styles['left']} style={{transform: `rotateY(-90deg) translateZ(${size/2}px)`}}/>
			<div className={styles['top']} style={{transform: `rotateX(90deg) translateZ(${size/2}px) rotateZ(90deg)`}}/>
			<div className={styles['bottom']} style={{transform: `rotateX(-90deg) translateZ(${size/2}px) rotateZ(90deg)`}}/>
			{props.children}
		</div>
	)
}

export default Cube;