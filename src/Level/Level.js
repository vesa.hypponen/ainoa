import React, { Component } from 'react';
import classnames from 'classnames';

import ReactGA from 'react-ga4';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowsAltV, faArrowsAlt, faSyncAlt } from '@fortawesome/free-solid-svg-icons'

import { MODE_BUILD, MODE_PLAY, MOVE_SPEED, BLOCK_SIZE, PERSPECTIVE } from './constants.js';

import styles from './Level.module.scss';
//import blockStyles from './Blocks.module.scss';

import Gestures from '../Gestures/Gestures.js';
import Joystick from '../Joystick/Joystick.js';

import Assembly from '../Assembly/Assembly.js';
import BlockSlot from '../BlockSlot/BlockSlot.js';
// import SkyBox from '../SkyBox/SkyBox.js'
import WorldItems from '../WorldItems/WorldItems.js';
import Camera from '../Camera/Camera.js';
import BlockSelector from '../BlockSelector/BlockSelector.js';

import empty from './empty.json';

import { World } from 'oimo';

// function handleInput() {
// 	// wsad = liiku
// 	// space = ylös
// 	// cmd = alas
// 	// q = poista palikka
// 	// e = pistä palikka
// }

class Level extends Component {
	constructor(props) {
		super(props);

		this.camera = new Camera({
			position: {
				x: BLOCK_SIZE * 5,
				y: BLOCK_SIZE * 5,
				z: 0
			},
			rotation: {
				x: -45,
				y: 90,
				z: 0
			}
		});
		this.keyState = {};
		this.levelRef = React.createRef();
		this.cameraRef = React.createRef();
		this.joystickPanRef = React.createRef();
		this.joystickMove = {x: 0, y:0};
		this.joystickPan = {x: 0, y:0};
		this.joystickUp = {x: 0, y:0};

		this.blockColors = [
			{name: 'environment', x: 0, y: 0},
			{name: 'environment', x: 1, y: 0},
			{name: 'environment', x: 2, y: 0},
			{name: 'environment', x: 3, y: 0},
			{name: 'environment', x: 4, y: 0},
			{name: 'environment', x: 5, y: 0},
			{name: 'environment', x: 6, y: 0},
			{name: 'environment', x: 7, y: 0},
			{name: 'environment', x: 9, y: 0},
		]

		this.selectedBlock = null;
		this.selectedBlockSlot = 1;

		this.state = {
			loaded: false,
			joystickPanVisible: false,
			mode: MODE_BUILD,
			showBlocks: false,
		}

		const {user} = this.props.match.params;

		const localLevel = window.localStorage.getItem(`level_${user}`) || JSON.stringify(empty);

		this.levelData = JSON.parse(localLevel);

		this.world = new World({
			timestep: 1/60,
			iterations: 8,
			broadphase: 2,
			worldscale: 1,
			random: true,
			info: true,
			gravity: [0,9.81,0]
		});
		this.world.add({ size:[50, 0.1, 50], pos:[0,0,0] }); // ground

		console.log('USER', user);
	}

	componentDidMount() {
		this.keyDownListener = window.addEventListener('keydown', this.keyDown);
		this.keyUpListener = window.addEventListener('keyup', this.keyUp);
		
		this.resizeListener = window.addEventListener('resize', this.resize);

		this.lastTick = Date.now();
		this.tickRequest = window.requestAnimationFrame(this.tick);

		this.loadLevel();
	}

	componentWillUnmount() {
		window.removeEventListener('keyup', this.keyUpListener);
		window.removeEventListener('keydown', this.keyDownListener);

		window.removeEventListener('resize', this.resizeListener);

		window.cancelAnimationFrame(this.tickRequest);
	}

	tick = () => {
		const {KeyW, KeyS, KeyA, KeyD, Space, MetaLeft} = this.keyState;

		const timeSinceLastTick = Date.now() - this.lastTick;
		const tickMove = timeSinceLastTick*MOVE_SPEED;
		const joystickPanThrottle = 200;
		const joystickThrottle = 100;

		// Calculate 'forward' based on camera angle
		const angleRad = this.camera.rotation.y * (Math.PI / 180)
		const forwardXmove = Math.sin(angleRad) * tickMove;
		const forwardZmove = Math.cos(angleRad) * tickMove;

		this.lastTick = Date.now();
		this.world.step();

		this.levelData.forEach( (box) => {
			if (!box.worldBox) return;
			//console.log('box:', box, ' - position:', box.worldBox.getPosition());
			const position = box.worldBox.getPosition();
			//console.log(box, position);
			box.x = position.x;
			box.y = position.y;
			box.z = position.z;
		});

		// Keyboard
		if (KeyW) {
			this.camera.position.z += (forwardZmove);
			this.camera.position.x -= (forwardXmove);
		}
		if (KeyS) {
			this.camera.position.z -= (forwardZmove);
			this.camera.position.x += (forwardXmove);
		}
		if (KeyA) {
			this.camera.position.x += (forwardZmove);
			this.camera.position.z += (forwardXmove);
		}
		if (KeyD) {
			this.camera.position.x -= (forwardZmove);
			this.camera.position.z -= (forwardXmove);
			
		}
		if (Space) this.camera.position.y += (tickMove);
		if (MetaLeft) this.camera.position.y -= (tickMove);

		//console.log(selectedBlock, mouseDownTime)

		// Move joystick
		if (this.joystickMove.y) {
			this.camera.position.z -= ((this.joystickMove.y/joystickThrottle)*(forwardZmove))
			this.camera.position.x += ((this.joystickMove.y/joystickThrottle)*(forwardXmove));
		}
		if (this.joystickMove.x) {
			this.camera.position.x -= ((this.joystickMove.x/joystickThrottle)*(forwardZmove))
			this.camera.position.z -= ((this.joystickMove.x/joystickThrottle)*(forwardXmove))
		}

		// Pan joystick
		if (this.joystickPan.y) {
			this.camera.rotation.x -= ((this.joystickPan.y/joystickPanThrottle)*(tickMove))
		}
		if (this.joystickPan.x) {
			this.camera.rotation.y += ((this.joystickPan.x/joystickPanThrottle)*(tickMove))
		}

		// Up joystick
		if (this.joystickUp.y) {
			this.camera.position.y -= ((this.joystickUp.y/joystickThrottle)*(tickMove))
		}
		if (this.joystickUp.x) {
			//this.camera.position.y -= ((this.joystickUp.x/joystickThrottle)*(tickMove))
		}
		
		this.camera.rotation.x = Math.max(this.camera.rotation.x, -90);
		this.camera.rotation.x = Math.min(this.camera.rotation.x, 90);
		
		// Move player
		if (this.levelRef && this.levelRef.current) {
			this.levelRef.current.style.transform = this.levelStyle().transform;
		}

		// Rotate camera
		if (this.cameraRef && this.cameraRef.current) {
			this.cameraRef.current.style.transform = this.cameraStyle().transform;
		}

		// Pan joystick follow touch
		if (this.joystickPanRef && this.joystickPanRef.current) {
			this.joystickPanRef.current.style.top = this.joystickPanStyle().top;
			this.joystickPanRef.current.style.left = this.joystickPanStyle().left;
		}
		
		this.forceUpdate();

		this.tickRequest = window.requestAnimationFrame(this.tick);
	}

	keyDown = (event) => {
		// TODO move user based on key
		// console.log(event);
		this.keyState[event.code] = true;
		//window.requite

		let keyInt = parseInt(event.key, 10);

		if (keyInt > 0 && keyInt < 10) {
			this.selectBlockSlot(keyInt);
		}

		if (event.code === 'ShiftLeft') {
			this.removeCurrentBlock();
		}

		//event.preventDefault();
		
	}

	keyUp = (event) => {
		this.keyState[event.code] = false;
		//event.preventDefault();
	}

	resize = (event) => {
		//this.setState({})
		console.log(event);
	}

	levelStyle = () => {
		const { x, y, z } = this.camera.position;
		return {
			transform: `translate3d(${x}px, ${y}px, ${z}px) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skewX(0deg) skewY(0deg)`
		}
	}

	cameraStyle = () => {
		const { x, y, z } = this.camera.rotation;
		return {
			transform: `translate3d(0px, 0px, ${PERSPECTIVE}vmin) rotateX(${x}deg) rotateY(${y}deg) rotateZ(${z}deg)`
		}
	}

	joystickPanStyle = () => {
		const {absoluteX, absoluteY} = this.joystickPan;
		return {
			left: `${absoluteX}px`,
			top: `${absoluteY}px`,
		};
	}

	handlejoystickMove = (position) => {
		this.joystickMove = position;
	}

	handleJoystickUp = (position) => {
		this.joystickUp = position;
	}

	addBlock = (position, style) => {
		console.log('addBlock', position, style);
		this.levelData.push({
			x: position.x,
			y: position.y,
			z: position.z,
			style
		})
		this.updateLocalLevel();
	}

	selectBlockSlot = (index) => {
		this.selectedBlockSlot = index;
	}

	onBlockFocus = (block, nextBlockPosition) => {
		// console.log(block);
		this.selectedBlock = {
			block,
			nextBlockPosition
		};
	}

	onBlockBlur = (block) => {
		// console.log('onblockblur');
		this.selectedBlock = null;
	}

	removeCurrentBlock = () => {
		const {selectedBlock} = this;
		console.log('removeCurrentBlock', selectedBlock);
		if (!selectedBlock) return;
		const foundBlock = this.levelData.findIndex( (block) => {
			// console.log('compare', block, selectedBlock);
			return 	block.x === selectedBlock.block.x &&
					block.y === selectedBlock.block.y &&
					block.z === selectedBlock.block.z;
		});
		console.log('found matching block', foundBlock);
		if (!foundBlock) return;

		if (foundBlock.worldBox) foundBlock.worldBox.remove();
		this.levelData.splice(foundBlock, 1);

		this.updateLocalLevel();
	}

	updateLocalLevel = () => {
		console.log('updateLocalLevel');
		const {user} = this.props.match.params;

		this.levelData = this.levelData.map( (block, blockIndex) => {
			const cullFaces = this.levelData
				// find adjacent blocks
				.filter( (testBlock, testBlockIndex) => {
					if (blockIndex === testBlockIndex) {
						return false;
					}
					const xdiff = testBlock.x - block.x;
					const ydiff = testBlock.y - block.y;
					const zdiff = testBlock.z - block.z;
					return (Math.abs(xdiff) <= 1 && Math.abs(ydiff) <= 1 && Math.abs(zdiff) <= 1);
				// reduce to face-culls
				}).reduce( (reduction, adjacent) => {
					// front
					if (adjacent.x === block.x &&
						adjacent.y === block.y &&
						adjacent.z === block.z + 1) {
							reduction.front = true;
						}
					// back
					if (adjacent.x === block.x &&
						adjacent.y === block.y &&
						adjacent.z === block.z - 1) {
							reduction.back = true;
						}
					// top
					if (adjacent.x === block.x &&
						adjacent.y === block.y - 1 &&
						adjacent.z === block.z) {
							reduction.top = true;
						}
					// bottom
					if (adjacent.x === block.x &&
						adjacent.y === block.y + 1 &&
						adjacent.z === block.z) {
							reduction.bottom = true;
						}
					// left
					if (adjacent.x === block.x - 1 &&
						adjacent.y === block.y &&
						adjacent.z === block.z) {
							reduction.left = true;
						}
					// right
					if (adjacent.x === block.x + 1 &&
						adjacent.y === block.y &&
						adjacent.z === block.z) {
							reduction.right = true;
						}
	
					return reduction;
				}, {
					front: false,
					back: false,
					top: false,
					bottom: false,
					left: false,
					right: false,
				});
			// console.log('parent block: ', block, ' - cullFaces: ', cullFaces);
			block.cullFaces = cullFaces;
			return block;
		});

		const saveLevelData = this.levelData.map( (box) => {
			const {worldBox, cullFaces, ...dataToSave} = box;
			return dataToSave;
		});

		window.localStorage.setItem(`level_${user}`, JSON.stringify(saveLevelData));
		this.forceUpdate();
	}

	renderBlock = (block) => {
		const BlockElement = WorldItems[block.style];
		return (
			<WorldItems.BasicBlock onFocus={this.onBlockFocus} onBlur={this.onBlockBlur} key={`${block.x}${block.y}${block.z}`} size={BLOCK_SIZE} position={{x: block.x, y: block.y, z: block.z}} block={block}>
				{BlockElement && <BlockElement/>}
			</WorldItems.BasicBlock>
		)
	}
	
	renderLevel = (mapData) => {
		const cubes = mapData.map( this.renderBlock );
		return (
			<>{cubes}</>
		)
	}

	loadLevel = () => {
		const {user} = this.props.match.params;
		fetch(`https://ainoa-levels.s3.eu-central-1.amazonaws.com/users/${user}`, {
			method: 'GET',
		}).then(response => {
			return response.json()
		}).then(json => {
			console.log('get response', json);
			if (!json.length) {
				console.error('invalid response from server', json);
				return;
			}
			this.levelData = json;

			this.initializeWorldData();

			this.updateLocalLevel();
		});
	}

	saveLevel = () => {
		const {user} = this.props.match.params;
		const saveLevelData = this.levelData.map( (box) => {
			const {worldBox, cullFaces, ...dataToSave} = box;
			return dataToSave;
		});

		fetch(`https://eipabhv0ti.execute-api.eu-central-1.amazonaws.com/dev/levels/users/${user}`, {
			method: 'PUT',
			body: JSON.stringify(saveLevelData)
		}).then(response => {
			return response.json()
		}).then(json => {
			console.log('save response', json);
		});
	}
	
	onMoveStart = (data, event) => {
		const {absoluteX, absoluteY} = data;
		this.setState({
			joystickPanVisible: true,
			longPressActive: true
		});
		this.joystickPan = {
			x: 0,
			y: 0,
			absoluteX,
			absoluteY
		}
		event.preventDefault();
	}

	onMove = (data, event) => {
		const {x, y, absoluteX, absoluteY} = data;
		this.joystickPan = {
			x,
			y,
			absoluteX,
			absoluteY
		}
		event.preventDefault();
	}

	onMoveEnd = (data, event) => {
		const { selectedBlock, selectedBlockSlot }  = this;
		const { longPressActive } = this.state;
		// console.log('onMoveEnd',data,selectedBlock);
		const x = 0;
		const y = 0;
		this.joystickPan = {
			x,
			y
		};

		if (longPressActive && selectedBlock) {
			this.addBlock(selectedBlock.nextBlockPosition, this.blockColors[selectedBlockSlot-1]);
		}

		this.selectedBlock = null;

		this.setState({
			joystickPanVisible: false,
			longPressActive: false,
		});
		// console.log(data);
		event.preventDefault();
	}

	onLongPress = () => {
		console.log('onLongPress');
		this.removeCurrentBlock();
		this.selectedBlock = null;
		this.setState({
			joystickPanVisible: false,
			longPressActive: false,
		});
	}

	onLongPressCancelled = () => {
		this.selectedBlock = null;
		this.setState({
			longPressActive: false,
		});
	}
	
	initializeWorldData = () => {
		// Blocks don't budge
		this.levelData = this.levelData.map( (box) => {
			const options = {
				type: 'box',
				size: [1, 1, 1],
				pos: [box.x, box.y, box.z],
				density: 1,
				move: false
			};
	
			box.worldBox = this.world.add( options );
			console.log('add box:', box, options, 'worldbox: ', box.worldBox);
			return box;
		});
	}

	toggleMode = () => {
		const nextMode = this.state.mode === MODE_BUILD ? MODE_PLAY : MODE_BUILD;
		console.log('toggleMode', nextMode);
		this.setState({
			mode: nextMode
		});

		if (nextMode === MODE_PLAY) {
			this.camera.reset();
		}
	}

	showBlocks = () => {
		this.setState({
			showBlocks: !this.state.showBlocks
		});
	}

	selectBlock = (sheet, x, y) => {
		this.blockColors[this.selectedBlockSlot-1] = {name: sheet.name, x, y};
		
		ReactGA.event({
			category: "selectBlock",
			action: sheet.name,
			label: `x:${x}, y:${y}`, // optional
			nonInteraction: false, // optional, true/false
			transport: "beacon", // optional, beacon/xhr/image
		  });		
	}

	render() {
		const { selectedBlockSlot } = this;
		const { joystickPanVisible } = this.state;
		const blockSlots = Array(this.blockColors.length)
			.fill(null)
			.map( (item, index) => {
				const blockIndex = index+1;
				return <BlockSlot onClick={this.selectBlockSlot} sheet={this.blockColors[index]} blockIndex={blockIndex} selected={blockIndex === selectedBlockSlot} key={`blockSlot_${index}`}/>
			});
		return (
			<>
				<div className={styles.gameContainer}>
					<Gestures onLongPress={this.onLongPress} onLongPressCancelled={this.onLongPressCancelled} onMoveStart={this.onMoveStart} onMove={this.onMove} onMoveEnd={this.onMoveEnd}>
						<div className={styles.viewport} style={{perspective: `${PERSPECTIVE}vmin`}}>
							<div ref={this.cameraRef} style={this.cameraStyle()} className={styles.camera}>
								{/* <SkyBox/> */}
								<div ref={this.levelRef} style={this.levelStyle()} className={styles.level}>
										<Assembly>
											{this.renderLevel(this.levelData)}
										</Assembly>
								</div>
							</div>
						</div>
					</Gestures>
				</div>
				<div ref={this.joystickPanRef} style={this.joystickPanStyle()}className={classnames({
					[styles.joystickPan]: true,
					[styles.joystickPanVisible]: joystickPanVisible
				})}>
					<FontAwesomeIcon className={styles.moveIcon} icon={faSyncAlt} />
				</div>
				<div className={styles.joystickMove}>
					<Joystick onUpdate={this.handlejoystickMove}>
						<FontAwesomeIcon className={styles.moveIcon} icon={faArrowsAlt} />
					</Joystick>
				</div>
				<div className={styles.joystickUp}>
					<Joystick onUpdate={this.handleJoystickUp}>
						<FontAwesomeIcon className={styles.moveIcon} icon={faArrowsAltV} />
					</Joystick>
				</div>
				<div className={styles.blockContainer}>
					{blockSlots}
				</div>
				<div className={styles.menu}>
					<div className={styles.menuItems}>
						<div onClick={this.showBlocks}>Choose blocks</div>
						<div onClick={this.toggleMode}>Mode: {this.state.mode}</div>
						<div onClick={this.saveLevel}>Save level</div>
					</div>
				</div>
				{this.state.showBlocks && <BlockSelector selectBlock={this.selectBlock}/>}
			</>
		)
	}
}

export default Level;