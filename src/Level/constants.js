export const MODE_BUILD = 'MODE_BUILD';
export const MODE_PLAY = 'MODE_PLAY';
export const MOVE_SPEED = 0.1; // pixels / ms
export const BLOCK_SIZE = 16; // in pixels
export const PERSPECTIVE = 59.5714285729; // in vmin
