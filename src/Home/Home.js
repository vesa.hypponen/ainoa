import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import styles from './Home.module.scss';

class Home extends Component {

	constructor(props) {
		super(props);
		this.state = {
			name: window.localStorage.getItem('name') || ''
		}
	}

	go = (history, event) => {
		console.log('go', history, event)
		const {name} = this.state;
		window.localStorage.setItem('name', name);
		history.push(`/level/${name}`);
	}

	onNameChange = (event) => {
		console.log(event.nativeEvent.target.value);
		this.setState({
			name: event.nativeEvent.target.value
		})
	}

	render() {
		const {name} = this.state;

		const Button = withRouter(({ history }) => (
			<button type='button' onClick={this.go.bind(null, history)}>
			  GO
			</button>
		));

		return(
			<div className={styles.home}>
				<h1 className={styles.header}>Ainoa</h1>
				<div className={styles.form}>
					<input onChange={this.onNameChange} value={name} maxLength={10} ref={(name) => this.name = name} placeholder='name' type='text'/>
					<Button/>
				</div>
			</div>
		)
	}
};

export default Home;