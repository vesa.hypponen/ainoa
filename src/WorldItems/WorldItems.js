import BasicBlock from './BasicBlock/BasicBlock.js';
import Pig from './Pig/Pig.js';

const WorldItems = {
	BasicBlock,
	Pig,
}

export default WorldItems;