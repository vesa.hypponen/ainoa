import React from 'react';
import styles from '../../Cube/Cube.module.scss';
// import basicStyles from './BasicBlock.module.scss';
import classnames from 'classnames';
import blockStyles from '../../BlockSelector/BlockSelector.module.scss';

function BasicBlock (props) {
	const {x,y,z} = props.position;
	let {size, onFocus, onBlur, block} = props;
	let { cullFaces, style } = block;
	cullFaces = cullFaces || {};

	style = style || {
		name: 'environment',
		x: 0,
		y: 0,
	};

	// console.log('render block', block);

	function getNewBlockPosition(pos) {
		const xadd = pos.x || 0;
		const yadd = pos.y || 0;
		const zadd = pos.z || 0;
		return {
			x: x + xadd,
			y: y + yadd,
			z: z + zadd
		}
	}

	const frontFocus = onFocus.bind(null, props.position, getNewBlockPosition({z: 1}));
	const rightFocus = onFocus.bind(null, props.position, getNewBlockPosition({x: 1}));
	const backFocus = onFocus.bind(null, props.position, getNewBlockPosition({z: -1}));
	const leftFocus = onFocus.bind(null, props.position, getNewBlockPosition({x: -1}));
	const topFocus = onFocus.bind(null, props.position, getNewBlockPosition({y: -1}));
	const bottomFocus = onFocus.bind(null, props.position, getNewBlockPosition({y: 1}));

	function blockClass(side) {
		const sheetStyle = blockStyles[style.name];
		return classnames({
			[styles[side]]: true,
			[sheetStyle]: Boolean(sheetStyle),
		});
	}

	function positionStyle() {
		return `-${style.x*(size+1)}px -${style.y*(size+1)}px`		
	}

	// TODO: add each face to have correct class & offset based on styles {name,x,y}
	return (
		<div className={styles.cube} style={{ width: `${size}px`, height: `${size}px`, transform: `translateX(${x*size}px) translateY(${y*size}px) translateZ(${z*size-size/2}px)` }}>
			{!cullFaces.front && <div onMouseEnter={frontFocus} onMouseLeave={onBlur} onTouchStart={frontFocus} className={blockClass('front')} style={{transform: `rotateY(0deg) translateZ(${size/2}px)`, backgroundPosition: positionStyle()}}/>}
			{!cullFaces.right && <div onMouseEnter={rightFocus} onMouseLeave={onBlur} onTouchStart={rightFocus} className={blockClass('right')} style={{transform: `rotateY(90deg) translateZ(${size/2}px)`, backgroundPosition: positionStyle()}}/>}
			{!cullFaces.back && <div onMouseEnter={backFocus} onMouseLeave={onBlur} onTouchStart={backFocus} className={blockClass('back')} style={{transform: `rotateY(180deg) translateZ(${size/2}px)`, backgroundPosition: positionStyle()}}/>}
			{!cullFaces.left && <div onMouseEnter={leftFocus} onMouseLeave={onBlur} onTouchStart={leftFocus} className={blockClass('left')} style={{transform: `rotateY(-90deg) translateZ(${size/2}px)`, backgroundPosition: positionStyle()}}/>}
			{!cullFaces.top && <div onMouseEnter={topFocus} onMouseLeave={onBlur} onTouchStart={topFocus} className={blockClass('top')} style={{transform: `rotateX(90deg) translateZ(${size/2}px) rotateZ(90deg)`, backgroundPosition: positionStyle()}}/>}
			{!cullFaces.bottom && <div onMouseEnter={bottomFocus} onMouseLeave={onBlur} onTouchStart={bottomFocus} className={blockClass('bottom')} style={{transform: `rotateX(-90deg) translateZ(${size/2}px) rotateZ(90deg)`, backgroundPosition: positionStyle()}}/>}
			{props.children}
		</div>
	)
}
function areEqual(prevProps, nextProps) {
	return  prevProps.x === nextProps.x &&
			prevProps.y === nextProps.y &&
			prevProps.z === nextProps.z &&
			prevProps.cullFaces && nextProps.cullFaces &&
			prevProps.cullFaces.top === nextProps.cullFaces.top &&
			prevProps.cullFaces.bottom === nextProps.cullFaces.bottom &&
			prevProps.cullFaces.back === nextProps.cullFaces.back &&
			prevProps.cullFaces.front === nextProps.cullFaces.front &&
			prevProps.cullFaces.left === nextProps.cullFaces.left &&
			prevProps.cullFaces.right === nextProps.cullFaces.right;
/*
return true if passing nextProps to render would return
the same result as passing prevProps to render,
otherwise return false
*/
}

export default React.memo(BasicBlock, areEqual);
//export default BasicBlock;