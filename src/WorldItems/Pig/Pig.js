import React, { Component } from 'react';

import Assembly from '../../Assembly/Assembly.js';

import Cube from '../../Cube/Cube.js';
import styles from './Pig.module.scss';

class Pig extends Component {

	render() {
		return (
			<Assembly className={styles.Pig}>
				<Assembly className={styles.Head}>
					<Cube size={15}/>
				</Assembly>
				<Assembly className={styles.Body}>
					<Cube size={25}/>
					<Cube size={5} className={styles.Tail}/>
					<Cube size={7.5} className={styles.LegFrontLeft}/>
					<Cube size={7.5} className={styles.LegFrontRight}/>
					<Cube size={7.5} className={styles.LegBackLeft}/>
					<Cube size={7.5} className={styles.LegBackRight}/>
				</Assembly>

				{/* <Cube size={15} position={{x: -1, y: 0, z: 0}}/>
				<Cube size={15} position={{x: 0, y: 0, z: 0}}/>

				<Cube size={7.5} position={{x: -2, y: 2, z: 0.2}}/>
				<Cube size={7.5} position={{x: -2, y: 2, z: -1.2}}/>

				<Cube size={7.5} position={{x: 1, y: 2, z: 0.2}}/>
				<Cube size={7.5} position={{x: 1, y: 2, z: -1.2}}/> */}
			</Assembly>
		)
	}
}

export default Pig;
