import React, { Component } from 'react';

import styles from './BlockSelector.module.scss';
import classnames from 'classnames';


class BlockSelector extends Component {
	constructor(props) {
        super(props);

        this.blockSize = 16;

        this.sheets = [{
            name: 'environment',
            size: [57, 31]
        },{
            name: 'characters',
            size: [54, 12]
        },{
            name: 'indoor',
            size: [27, 18]
        },{
            name: 'dungeon',
            size: [29, 18]
        }];

        this.state = {
            openSheet: 0,
        };
    }

    render() {
        const tabs = this.sheets.map( (sheet, index) => {
            return (
                <div className={styles.tab}>
                    <div onClick={() => {
                        this.setState({openSheet: index})
                    }} className={classnames({
                        [styles[sheet.name]]: true,
                        [styles.selected]: this.state.openSheet === index
                    })} style={{'backgroundPosition': '0px 0px'}}></div>
                </div>
            )
        });

        const sheet = this.sheets[this.state.openSheet];
        let blocks = [];
        for (let y=0; y<sheet.size[1]; y++) {
            for (let x=0; x<sheet.size[0]; x++) {
                blocks.push(<div onClick={() => {
                    this.props.selectBlock(sheet, x, y);
                }} className={styles.block}>
                    <div key={`${sheet.name}${y}.${x}`} className={styles[sheet.name]} style={{'backgroundPosition': `-${x*(this.blockSize+1)}px -${y*(this.blockSize+1)}px`}}></div>
                </div>)
            }
        }

        return (
            <div className={styles.container}>
                <div className={styles.tabContainer}>
                    {tabs}
                </div>
                <div className={styles.blockContainer}>
                    <div className={styles.blockScroller} style={{'maxWidth': `${(this.blockSize)*sheet.size[0]}px`,'minWidth': `${(this.blockSize)*sheet.size[0]}px`}}>
                        {blocks}
                    </div>
                </div>
            </div>
        )
    }
}


export default BlockSelector;