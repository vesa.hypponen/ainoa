
class Camera {

	constructor(settings) {
		console.log(settings);
		let { position, rotation } = settings;
		this.position = position || this.vec0();
		this.rotation = rotation || this.vec0();
		this.resetPosition = Object.assign({}, this.position);
		this.resetRotation = Object.assign({}, this.rotation);
		console.log('init camera', this.position, this.rotation);
	}

	vec0 = () => {
		return {x: 0, y: 0, z: 0};
	}

	reset = () => {
		this.position = Object.assign({}, this.resetPosition || this.vec0());
		this.rotation = Object.assign({}, this.resetRotation || this.vec0());
	}
}

export default Camera;