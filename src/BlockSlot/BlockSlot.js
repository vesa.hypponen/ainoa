import React from 'react';
import styles from './BlockSlot.module.scss';
import blockStyles from '../BlockSelector/BlockSelector.module.scss';
import classnames from 'classnames';

function BlockSlot (props) {
	const { blockIndex, selected, onClick, sheet } = props;
	const blockSize = 16;

	function selectBlock () {
		onClick(blockIndex);
	}
	
	return (
		<div onClick={selectBlock} className={classnames({
			[styles.slot]: true,
			[styles.selected]: selected
		})}>
			<div className={blockStyles[sheet.name]} style={{'backgroundPosition': `-${sheet.x*(blockSize+1)}px -${sheet.y*(blockSize+1)}px`}}/>
		</div>
	)
}

export default BlockSlot;