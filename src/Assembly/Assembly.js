import React from 'react';
import styles from './Assembly.module.scss';
import classnames from 'classnames';

function Assembly (props) {
	
	return (
		<div {...props} className={classnames({
			[styles.outer]: true,
			[props.className]: Boolean(props.className)
		})}>
			{props.children}
		</div>
	)
}

export default Assembly;